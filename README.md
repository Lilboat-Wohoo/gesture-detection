# Gesture Detection with PaddleDetection



## Getting started

### 1 git clone PaddleDetection
```
git clone https://github.com/PaddlePaddle/PaddleDetection.git
```
### 2 create output_inference folder

## 打电话检测

### 1 下载行人检测模型
```
下载链接:https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip
```
### 2 下载打电话检测模型
```
下载链接:hhttps://bj.bcebos.com/v1/paddledet/models/pipeline/PPHGNet_tiny_calling_halfbody.zip
```
### 3 通过terminal使用模型
```
    1.从模型库中下载行人检测/跟踪、打电话行为识别两个预测部署模型并解压到./output_inference路径下；默认自动下载模型，如果手动下载，需要修改模型文件夹为模型存放路径。
    2.修改配置文件deploy/pipeline/config/infer_cfg_pphuman.yml中ID_BASED_CLSACTION下的enable为True；
    3.仅支持输入视频，启动命令如下：python deploy/pipeline/pipeline.py --config deploy/pipeline/config/infer_cfg_pphuman.yml \
                                                   --video_file=test_video.mp4 \
                                                   --device=gpu

```
### id说明
```
0：打电话
1：其他
```
## 抽烟检测
### 1.下载行人检测模型
```
下载链接:https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip
```
### 2 下载抽烟检测模型
```
下载链接:https://bj.bcebos.com/v1/paddledet/models/pipeline/ppyoloe_crn_s_80e_smoking_visdrone.zip
```
### 3 通过terminal使用模型
```
    1.从模型库中下载行人检测/跟踪、打电话行为识别两个预测部署模型并解压到./output_inference路径下；默认自动下载模型，如果手动下载，需要修改模型文件夹为模型存放路径。
    2.修改配置文件deploy/pipeline/config/infer_cfg_pphuman.yml中ID_BASED_CLSACTION下的enable为True；
    3.仅支持输入视频，启动命令如下：python deploy/pipeline/pipeline.py --config deploy/pipeline/config/infer_cfg_pphuman.yml \
                                                   --video_file=test_video.mp4 \
                                                   --device=gpu
```
### id说明
```
0：抽了
1：其他
```
